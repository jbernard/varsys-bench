#!/usr/bin/env python3

import multiprocessing
import os
import socket
import subprocess
import sys
import time
import timeit


def set_frequency(frequency, delay):
    ret = subprocess.call(['sudo', 'cpufreq-set', '-f', str(frequency)])
    if ret != 0:
        print("ERROR: failed to set CPU frequency to %d" % frequency)
        exit(-1)

    print("DEBUG: set frequency to %d" % frequency)

    # Allow time for new frequency to settle
    time.sleep(delay)


def run_kmeans(outpath, frequency, size, nthreads, iteration):

    cmd = ['/usr/bin/numactl', '-l',
           '/home/jbernard/run/kmeans',
           '-n', str(nthreads),
           '-i', '/home/jbernard/kmeans-data/%s' % (PROBLEM_SIZE[size])] 

    print('DEBUG: - %s' % cmd)

    with open("%s/%s-%s-%s-%s.out" % (
            outpath, frequency, size, nthreads, iteration), 'w') as output:

        ret = subprocess.call(cmd, stdout=output, stderr=output)
        if ret != 0:
            print("ERROR: benchmark failed")
            exit(-1)


# Who am I?
MY_HOSTNAME = socket.gethostname().split('.')[0]

# Location to store iozone output.
OUTPUTPATH = "/home/jbernard/results/rodinia/kmeans/%s" % MY_HOSTNAME

# kmeans problem size
PROBLEM_SIZE = {
    "small": "100",
    "medium": "204800.txt",
    "large": "819200.txt"
}

# Use same number of threads as CPU cores.
NUM_THREADS = [8, 12, 16]

# Number of tests to run for each configuration.
ITERATIONS = 25

# Amount of time to wait for frequency to settle.
FREQUENCY_DELAY_SEC = 1

# Available CPU frequencies (moana nodes).
FREQUENCIES_MOANA = [
    3500000,
    # 3400000,
    # 3200000,
    # 3100000,
    2900000,
    # 2800000,
    # 2600000,
    # 2500000,
    # 2300000,
    # 2200000,
    2000000,
    # 1900000,
    # 1700000,
    # 1600000,
    # 1400000,
    1200000]

# Available CPU frequencies (wendy nodes).
FREQUENCIES_WENDY = [
    3000000,
    # 2900000,
    # 2800000,
    # 2700000,
    2600000,
    # 2400000,
    # 2300000,
    # 2200000,
    # 2100000,
    # 2000000,
    1800000,
    # 1700000,
    # 1600000,
    # 1500000,
    # 1400000,
    1200000]


if MY_HOSTNAME.startswith('m'):
    FREQUENCIES = FREQUENCIES_MOANA
    print("INFO: using moana frequency map")
elif MY_HOSTNAME.startswith('w'):
    FREQUENCIES = FREQUENCIES_WENDY
    print("INFO: using wendy frequency map")
else:
    print("ERROR: unknown hostname \"%s\"" % MY_HOSTNAME)
    exit(-1)


print("INFO: setting cpu frequency governor to \"userspace\"...")
ret = subprocess.call(['sudo', 'cpufreq-set', '-g', 'userspace'])
if ret != 0:
    print("ERROR: failed to set CPU governor to \"userspace\"")
    exit(-1)


if not os.path.exists(OUTPUTPATH):
    os.makedirs(OUTPUTPATH)


for frequency in FREQUENCIES:
    set_frequency(frequency, FREQUENCY_DELAY_SEC)
    for size in PROBLEM_SIZE:
        for num_threads in NUM_THREADS:
            for i in list(range(ITERATIONS)):
                run_kmeans(OUTPUTPATH, frequency, size, num_threads, i)


print("INFO: all tests completed, exiting")
